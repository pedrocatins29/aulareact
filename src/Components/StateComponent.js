import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default class StateComponent extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>
          O texto enviado da outra pagina foi:
          {JSON.stringify(navigation.getParam('text'))}
        </Text>
        <Text>
          A mensagem foi: {JSON.stringify(navigation.getParam('mensagem'))}
        </Text>
      </View>
    );
  }
}
