import React, { Component } from 'react';
import { Text, TextInput, View } from 'react-native';

export default class Change extends Component {
  constructor(props) {
    super(props);
    this.state = {text: ''};
  }

  render() {
    return (
      <View style={{padding: 10}}>
        <TextInput
          style={{height: 40}}
          placeholder="Digite aqui para traduzir!"
          onChangeText={text => this.setState({text})}
          value={this.state.text}
        />
        <Text style={{padding: 10, fontSize: 42}}>
          {this.state.text
            .split(' ')
            .map(word => word && '🍕')
            .join(' ')}
        </Text>
        <Text style={{padding: 10, fontSize: 42}}>{this.state.text}</Text>
      </View>
    );
  }
}
