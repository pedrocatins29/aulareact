import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default class PropsComponent extends Component {
  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <Text>Olá {this.props.name}!</Text>
      </View>
    );
  }
}
