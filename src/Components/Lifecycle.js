import React, {Component} from 'react';
import {Text, View, TextInput} from 'react-native';

export default class Lifecycle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: 'Ola mundo',
    };
  }

  // componentWillMount() {
  //   fetch('https://pokeapi.co/api/v2/pokemon/ditto/')
  //     .then(res => res.json())
  //     .then(res => console.log(res));
  // }

  // componentDidMount() {
  //   this.setState({
  //     message: 'Eu fui mudado',
  //   });
  // }

  // componentWillUnmount() {
  //   console.log('O componente foi fechado');
  // }

  // componentDidUpdate() {
  //   console.log('mudooooouuuuu');
  // }

  render() {
    return (
      <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
        {/* <TextInput
          style={{height: 40}}
          placeholder="Digite uma mensagem para refazer a pagina"
          onChangeText={message => this.setState({message})}
          value={this.state.message}
        /> */}
        <Text>{this.state.message}</Text>
      </View>
    );
  }
}
