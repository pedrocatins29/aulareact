import React, {Component} from 'react';
import {Button, View} from 'react-native';

export default class Home extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View
        style={{
          justifyContent: 'space-evenly',
          flex: 1,
          backgroundColor: 'black',
        }}>
        <Button title="Click" onPress={() => navigate('TelaClick')} />
        <Button title="Change" onPress={() => navigate('TelaChange')} />
        <Button title="Lifecycle" onPress={() => navigate('TelaCiclo')} />
        <Button title="ParamsRoutes" onPress={() => navigate('ParamsRoutes')} />
      </View>
    );
  }
}
