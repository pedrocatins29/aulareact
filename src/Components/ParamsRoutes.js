import React, {Component} from 'react';
import {Text, View, Button, TextInput} from 'react-native';

export default class ParamsRoutes extends Component {
  constructor(props) {
    super(props);
    this.state = {text: ''};
  }
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <TextInput
          style={{height: 40}}
          placeholder="Digite uma mensagem para mandar para outra pagina"
          onChangeText={text => this.setState({text})}
          value={this.state.text}
        />
        <Button
          onPress={() =>
            navigate('StateComponent', {
              text: this.state.text,
              mensagem: 'uma mensagem linda da pagina de ParamsRoutes',
            })
          }
          title="Passe os dados escritos para outra pagina"
        />
      </View>
    );
  }
}
