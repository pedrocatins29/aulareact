import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Change from './Components/Change';
import Click from './Components/Click';
import Home from './Components/Home';
import BlinkApp from './Components/Lifecycle';
import PropsComponent from './Components/PropsComponent';
import StateComponent from './Components/StateComponent';
import ParamsRoutes from './Components/ParamsRoutes';

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    TelaState: StateComponent,
    TelaProps: PropsComponent,
    TelaClick: Click,
    TelaChange: Change,
    TelaCiclo: BlinkApp,
    ParamsRoutes: ParamsRoutes,
    StateComponent: StateComponent,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

export default (Router = createAppContainer(AppNavigator));
