# State

Existem dois tipos de dados que controlam um componente: props e state.
props são definidos pelo parent e são corrigidos durante a vida útil de um componente.
Para dados que vão mudar, precisamos usar state.
quando setState e chamado a funcao refaz o componente

# Props

A maioria dos componentes pode ser personalizada quando criados,
com parâmetros diferentes. Esses parâmetros de criação são chamados de props,
abreviação de propriedades.

# Ordem

- React native o que é
- React native init
- Estrutura de pastas e oque cada arquivo faz
- Organização de estrutura de pastas(Como eu gosto de fazer)
- O que é um componente Funcional e de classe (Brevemente)
- Basico JSX (integracao de tags com JS)
- Style usando js(StyleSheet)
- State
- Props
- lifecycle methods
  - componentDidMount() = chamado quando criado e inserido no DOM
  - componentDidUpdate() = chamado quando um tem mudancas no props ou no state
  - componentWillUnmount() = chamado quando um componente e removido do DOM
- Funções basica de onClick e handleChange (Two way data binding, Evento de clique de um botao)
- React navigation stack e tudo sobre rotas(Juntando todas as telas que fizermos com botoes e rotas)
